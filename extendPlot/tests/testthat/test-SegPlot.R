# pour bien faire les choses il faudrait pouvoir faire du contrôle de ce qui est plotté
# au moins pourrait sortir les coordonnées des différents éléments et vérifier que ça colle

test_that("CatPlot et SegPlot ok",{
# fake data
Nseg <- 15
base <- seq(0.1,1,length.out=Nseg)
min <- base*2
max <- base*5
median <- base*4
names <- letters[1:Nseg]
groups <- rep(c("First","Second","Third"),each=5)
# plotting
par(mfrow=c(2,2),mar=c(5.1,2.1,4.1,1.1)) # some fancy arrangement
SegBarPlot(median,min,max,names=names,main="SegBarPlot")
SegBarPlot(median,min,max,comp=median+0.2,names=names,main="SegBarPlot")
CatPlot(median,min,max,groups=groups,TV=1,names=names,main="CatPlot with names")
CatPlot(median,min,max,groups=groups,TV=1,main="CatPlot without names",groupLine=0.1)

dev.new()
# par(mar=c(5.1,2.1,4.1,1.1)) # some fancy arrangement
# layout(matrix(c(1,2,2,2,3,3,3),nrow=1))
# plot.new()
par(mfrow=c(1,2))
CatPlot(median,min,max,groups=groups,groupNames=groups,TV=1,names=names,main="CatPlot with names")
CatPlot(median,min,max,groups=groups,groupNames=NULL,TV=1,main="CatPlot without names")

})
